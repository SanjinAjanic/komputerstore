const loanElement = document.getElementById("loan-btn");
const workElement = document.getElementById("work-btn");
const bankElement = document.getElementById("bank-btn");
const getLoanElement = document.getElementById("balance");
const laptopElement = document.getElementById("computers");
const menuElement = document.getElementById("menu");
const imageElement = document.getElementById("laptop.image");
const buyComputerElement = document.getElementById("buy-computer");
const payBackLoanElement = document.getElementById("pay-back-btn");


let computerId = 0;
let balance = 0;
let loan = 0;
let salary = 0;
let bank = 0;
let computersArray = [];
let features = [];

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => computerInfo(computers));
const computerInfo = (computers) => {
    computersArray = this.computers;
    for (const computer of computers) {
        laptopElement1 = document.createElement("option");
        laptopElement1.value = computer.title;
        laptopElement1.appendChild(document.createTextNode(computer.title));
        laptopElement.appendChild(laptopElement1);    
    }
}
// function that handels the menu change of selected computer in the dropdown whit right elements
const handleComputerMenuChange = e => {
    menuElement.replaceChildren([]);
    const selectedComputer = computersArray[e.target.selectedIndex];
    computerId = e.target.selectedIndex;
    features.push(selectedComputer.description)
    features.push(selectedComputer.specs[0])
    features.push(selectedComputer.specs[1])
    features.push(selectedComputer.price)


    for (const feature of features) {
        const laptopElement2 = document.createElement("li");
        laptopElement2.innerText = feature;
        menuElement.appendChild(laptopElement2);
        imageElement.src = `https://noroff-komputer-store-api.herokuapp.com/${selectedComputer.image}`
    }
    features = [];
}
// function for balance and active loans and payback 10% from work if active loane exist.
function bankAccout() {

    if (loan > 0) {
        if(loan < salary){
            salary = loan-salary;
            balance += salary;
            loan = 0;
            salary = 0;
            updateBalance();
        }
        else{
            let procentageValue = salary * 0.1;
            loan -= procentageValue;
            salary -= procentageValue;
            balance += salary;
            salary = 0;
            updateBalance();
        }
       
    }

    balance += salary;
    document.getElementById("balance").innerHTML = "Balance : " + balance;
    let element = document.getElementById("work");
    element.innerHTML = "";
    salary = 0;
}
// function that adds 100 in salary by clicking on work button
function work() {
    salary += 100;
    document.getElementById("work").innerHTML = "Pay :" + salary;
}

// function that checks if the balance is greather than the loan if true you can payback loan
function payBackLoan() {

    if (balance > loan) {
        let payBack = balance - loan;
        balance = payBack;
        loan = 0;
        document.getElementById("balance").innerHTML = "Balance : " + balance;
        document.getElementById("active-loan").innerHTML = `Active loan :  ${loan}`;
    }

    
   // balance = 0;
}
// function that checks if you can borrow money or not and if you have active loans.
function getLoan() {
    let amount = parseInt(prompt("How much do you want to borrow?"))
    console.log(balance)
    if (isNaN (amount) )
        return console.log("isNaN");
    if (loan > 0) {
        alert("you cant take more lons pay back first");
    }
    else if (amount <= balance * 2 ) {
        console.log("hej")
        getLoanElement.innerHTML = (balance + amount);
        loan = amount;
        getLoanElement.innerHTML = "";
        document.getElementById("active-loan").innerHTML = `Active loan :  ${loan}`;
        balance += loan;
        getLoanElement.innerHTML = `Balance: ${balance}`;
    }
}
// checking if the the balance on account is grether or equal whit the price if yes you can buy a computer else alert.
function buyComputer() {
    const price = computersArray[computerId].price;
    if (balance >= price) {

        balance -= price;
        getLoanElement.innerHTML = balance;
    }
    else {
        alert("You dont have money go back and work!")
    }
}
// function that updates balance.
function updateBalance(){
    document.getElementById("balance").innerHTML = "Balance : " + balance;
    document.getElementById("active-loan").innerHTML = `Active loan :  ${loan}`;
}

/// Eventlisteners for the click and change events
buyComputerElement.addEventListener("click", buyComputer);
workElement.addEventListener("click", work);
loanElement.addEventListener("click", getLoan);
bankElement.addEventListener("click", bankAccout);
laptopElement.addEventListener("change", handleComputerMenuChange);
payBackLoanElement.addEventListener("click", payBackLoan);

